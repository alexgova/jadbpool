# README #

### Just Another DB Pool (JADBPool). ###

* Implementation of a very basic DB Connection Pool.
* 0.0.1

### Usage ###

```java
JADBPoolConfiguration config = new JADBPoolConfiguration();
config.setMinAvailableItems(5);
config.setMaxCapacity(10);
config.setDatabaseUrl("jdbc:h2:mem:test"); // or jdbc valid url
//config.setUser("user");
//config.setPassword("***");

JADBPool pool = new JADBPool(config);

try (JADBPoolConnection connection = pool.getConnection()) {
    // ... use connection
}

pool.close();
```

### Basic Class Diagram ###
![Basic Class Diagram](https://bytebucket.org/alexgova/jadbpool/raw/c7cd31deeaf75d3fb2bd46e2de7914a9f3c8f8d8/doc/JADBPoolBasicClassDiagram.jpg)
