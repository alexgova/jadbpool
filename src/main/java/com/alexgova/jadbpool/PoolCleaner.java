package com.alexgova.jadbpool;

import java.util.List;
import java.util.concurrent.ExecutorService;

/**
 * Maintenance task used to remove INVALID items and exceeding AVAILABLE items
 * from the pool.
 */
final class PoolCleaner<T extends PoolItem> extends PoolMaintenanceTask<T> implements Runnable {

	public PoolCleaner(Pool<T> pool, PoolConfiguration config, ExecutorService executor) {
		super(pool, config, executor);
	}

	@Override
	public void run() {
		removeInvalidItems();

		removeExceedingAvailableItems();

		awaitTermination();
	}

	private void removeInvalidItems() {
		List<T> invalidItems = pool.getItemsByState(PoolItemState.INVALID);
		for (int i = 0; i < invalidItems.size(); i++) {
			final T item = invalidItems.get(i);
			executor.submit(() -> pool.removeItem(item));
		}
	}

	private void removeExceedingAvailableItems() {
		int itemsToRemove = pool.getCountByState(PoolItemState.AVAILABLE) - config.getMinAvailableItems();
		List<T> availableItems = pool.getItemsByState(PoolItemState.AVAILABLE);
		for (int i = availableItems.size() - 1; i > 0; i--) {
			final T item = availableItems.get(i);
			if (itemsToRemove > 0 && item.compareAndSet(PoolItemState.AVAILABLE, PoolItemState.INVALID)) {
				executor.submit(() -> pool.removeItem(item));
				itemsToRemove--;
			}
		}

	}

}
