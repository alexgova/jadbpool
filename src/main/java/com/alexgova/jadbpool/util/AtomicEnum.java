package com.alexgova.jadbpool.util;

import java.util.concurrent.atomic.AtomicReference;

public final class AtomicEnum<T extends Enum<T>> {

	private final AtomicReference<T> ref;

	public AtomicEnum(final T initialValue) {
		this.ref = new AtomicReference<>(initialValue);
	}

	public final void set(final T newValue) {
		this.ref.set(newValue);
	}

	public final T get() {
		return this.ref.get();
	}

	public final boolean compareAndSet(final T expect, final T update) {
		return this.ref.compareAndSet(expect, update);
	}

}
