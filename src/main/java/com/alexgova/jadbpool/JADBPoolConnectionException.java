package com.alexgova.jadbpool;

/**
 * Custom exception to be used by the JADBPool classes.
 */
public class JADBPoolConnectionException extends RuntimeException {

	private static final long serialVersionUID = 7820537757980336038L;

	public JADBPoolConnectionException() {
		super();
	}

	public JADBPoolConnectionException(String message) {
		super(message);
	}

	public JADBPoolConnectionException(String message, Throwable throwable) {
		super(message, throwable);
	}
}
