package com.alexgova.jadbpool;

public enum PoolItemState {
	UNAVAILABLE, AVAILABLE, INVALID
}
