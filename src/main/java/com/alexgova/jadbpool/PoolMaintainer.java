package com.alexgova.jadbpool;

import java.io.Closeable;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.ScheduledThreadPoolExecutor;
import java.util.concurrent.ThreadPoolExecutor;
import java.util.concurrent.TimeUnit;

/**
 * Used to do maintenance on the pool (clean, fill)
 */
class PoolMaintainer<T extends PoolItem> implements Closeable {

	private static final long FIRST_RUN_DELAY_MS = 100;
	private static final long MAINTENANCE_PERIOD_MS = 50;

	private ScheduledThreadPoolExecutor maintainerExecutor = new ScheduledThreadPoolExecutor(1,
			new ThreadPoolExecutor.DiscardPolicy());
	private ExecutorService tasksExecutor = Executors.newWorkStealingPool();
	private final List<Runnable> tasks = new ArrayList<>();
	private final Runnable fullCleaner;

	public PoolMaintainer(final Pool<T> pool, final PoolConfiguration config) {
		tasks.add(new PoolCleaner<T>(pool, config, tasksExecutor));
		tasks.add(new PoolFiller<T>(pool, config, tasksExecutor));
		fullCleaner = new PoolAllCleaner<T>(pool, config, tasksExecutor);

		maintainerExecutor.scheduleWithFixedDelay(new Worker(), FIRST_RUN_DELAY_MS, MAINTENANCE_PERIOD_MS,
				TimeUnit.MILLISECONDS);
	}

	private class Worker implements Runnable {

		@Override
		public void run() {
			for (int i = 0; i < tasks.size(); i++) {
				final Runnable task = tasks.get(i);
				maintainerExecutor.submit(task);
			}

		}

	}

	@Override
	public void close() throws IOException {
		maintainerExecutor.submit(fullCleaner);
		maintainerExecutor.shutdown();
		tasksExecutor.shutdown();
	}

}
