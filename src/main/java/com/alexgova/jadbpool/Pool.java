package com.alexgova.jadbpool;

import java.io.Closeable;
import java.io.IOException;
import java.util.Calendar;
import java.util.List;
import java.util.concurrent.CopyOnWriteArrayList;
import java.util.stream.Collectors;

/**
 * This is the base class you can extend if you want a Pool of custom objects,
 * along with PoolItem.
 * 
 * Contains PoolMaintainer to execute Pool Maintenance Tasks (Clean and Fill)
 * and PoolMetricsTracker to keep track of some statistics related to pool
 * usage.
 * 
 * When you extend this pool, you need to implement the createItem() method to
 * let the pool know how the items in the pool are created.
 * 
 */
public abstract class Pool<T extends PoolItem> implements Closeable {

	private final CopyOnWriteArrayList<T> pooledItems = new CopyOnWriteArrayList<>();
	private final PoolMaintainer<T> maintainer;
	private final PoolConfiguration config;
	private final PoolMetricsTracker<T> metricsTracker;

	/**
	 * Protected constructor, to create a pool, should use the public constructor in
	 * the extend class.
	 * 
	 * Will instantiate PoolMaintainer and PoolMetricsTracker.
	 * 
	 * @param config
	 *            the configuration of the pool containing minAvailableItems and
	 *            maxCapacity
	 */
	protected Pool(PoolConfiguration config) {
		// TODO validate configuration
		this.config = config;
		maintainer = new PoolMaintainer<>(this, config);
		metricsTracker = new PoolMetricsTracker<>();
	}

	protected void fillPool() {
		for (int i = 0; i < config.getMinAvailableItems(); i++) {
			T item = createItem();
			pooledItems.add(item);
		}

	}

	/**
	 * This is the method you should implement to let the pool know how to create
	 * items for the pool.
	 * 
	 * @return T the item created
	 */
	protected abstract T createItem();

	/**
	 * Will try to get an available item from the pool, if none found and max
	 * capacity of the pool hasn't been exceeded, will create one and return it.
	 * 
	 * 
	 * @return T a T item. @exception
	 * @exception PoolException
	 *                if unable to return an available item
	 */
	protected synchronized T getItem() {

		long now = Calendar.getInstance().getTimeInMillis();

		for (T item : pooledItems) {
			if (item.compareAndSet(PoolItemState.AVAILABLE, PoolItemState.UNAVAILABLE)) {
				item.setLastLeased(now);
				metricsTracker.recordItemLeased(Calendar.getInstance().getTimeInMillis() - now);
				return item;
			}
		}

		if (pooledItems.size() < config.getMaxCapacity()) {
			final T newItem = createItem();
			newItem.setState(PoolItemState.UNAVAILABLE);
			newItem.setLastLeased(now);
			addItemToPool(newItem);
			metricsTracker.recordItemLeased(Calendar.getInstance().getTimeInMillis() - now);
			return newItem;
		}

		throw new PoolException("Unable to get item from pool. Probably full.");
	}

	/**
	 * Will add an item to the pooledItems.
	 * 
	 * @param item
	 *            to add.
	 */
	protected void addItemToPool(T item) {
		pooledItems.add(item);
	}

	/**
	 * To be called after the item has been validated and can still be used.
	 * 
	 * @param item
	 *            to be returned to the pool of available items.
	 */
	protected void recyleItem(T item) {
		item.setState(PoolItemState.AVAILABLE);
		final long leasedTime = Calendar.getInstance().getTimeInMillis() - item.getLastLeased();
		metricsTracker.recordLeasedItemReturn(leasedTime);
	}

	/**
	 * To definitely remove items from the pool.
	 * 
	 * @param item
	 *            to be removed from the pool.
	 */
	protected void removeItem(final T item) {
		item.dispose();
		pooledItems.remove(item);
		metricsTracker.recordItemDisposed();
	}

	/**
	 * Given a state, will return the number of items in the pool that matches it.
	 * 
	 * This is a snapshot since PoolMaintainer and creation of new items can modify
	 * this number.
	 * 
	 * @param state
	 *            the state to be counted.
	 * @return int number of items int the pool with the given state
	 */
	public int getCountByState(PoolItemState state) {
		int count = 0;

		for (T item : pooledItems) {
			if (item.getState() == state)
				count++;
		}

		return count;

	}

	/**
	 * Will return a list of items filtered by the given state.
	 * 
	 * @param state
	 *            state of the wanted items.
	 * @return List list of items with given state.
	 */
	protected List<T> getItemsByState(PoolItemState state) {
		return pooledItems.stream().filter(i -> i.getState() == state).collect(Collectors.toList());
	}

	/**
	 * Will return all the items contained in the pool.
	 * 
	 * @return List list of all the items in the pool.
	 */
	protected List<T> getAllItems() {
		return pooledItems.stream().collect(Collectors.toList());
	}

	/**
	 * Returns the number of items currently in the queue independent of state
	 * 
	 * @return int the number of items in the pool
	 */
	public int size() {
		return pooledItems.size();
	}

	/**
	 * To release resources used by the pool.
	 */
	@Override
	public void close() throws IOException {
		maintainer.close();
	}

	/**
	 * 
	 * @return int the recorded number of items that have been disposed.
	 */
	public int getItemsDisposed() {
		return metricsTracker.getItemsDisposed();
	}

	/**
	 * 
	 * @return int the mean time an item has been leased from the pool.
	 */
	public float getMeanLeaseTime() {
		return metricsTracker.getMeanLeaseTime();
	}

	/**
	 * 
	 * @return int the mean time you have to wait for an item to be acquired from
	 *         the pool.
	 */
	public float getMeanWaitTime() {
		return metricsTracker.getMeanWaitTime();
	}

	/**
	 * Will instruct the metricsTracker to print out the stats it contains at the
	 * moment of the request.
	 */
	public void printStats() {
		metricsTracker.printStats(this);
	}
}
