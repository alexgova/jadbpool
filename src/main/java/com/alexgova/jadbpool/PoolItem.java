package com.alexgova.jadbpool;

import java.io.Closeable;
import java.io.IOException;

import com.alexgova.jadbpool.util.AtomicEnum;

/**
 * Base class used to be handled by the Pool.
 * 
 * When you create a custom Pool, this is the class you should extend so the
 * pool can handle your own items.
 */
public abstract class PoolItem implements Closeable {

	private AtomicEnum<PoolItemState> state = new AtomicEnum<>(PoolItemState.AVAILABLE);
	private long lastLeased;
	private final Pool pool;

	public PoolItem(Pool pool) {
		this.pool = pool;
	}

	public boolean compareAndSet(PoolItemState expectState, PoolItemState newState) {
		return state.compareAndSet(expectState, newState);
	}

	public void setState(PoolItemState newState) {
		state.set(newState);
	};

	public PoolItemState getState() {
		return state.get();
	}

	public abstract boolean isValid();

	public abstract void dispose();

	@Override
	public void close() throws IOException {
		if (isValid())
			pool.recyleItem(this);
		else
			pool.removeItem(this);

	}

	public long getLastLeased() {
		return lastLeased;
	}

	public void setLastLeased(long lastLeased) {
		this.lastLeased = lastLeased;
	}

}
