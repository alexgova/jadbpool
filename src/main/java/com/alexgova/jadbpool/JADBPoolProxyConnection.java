package com.alexgova.jadbpool;

import java.io.IOException;
import java.sql.Array;
import java.sql.Blob;
import java.sql.CallableStatement;
import java.sql.Clob;
import java.sql.Connection;
import java.sql.DatabaseMetaData;
import java.sql.NClob;
import java.sql.PreparedStatement;
import java.sql.SQLClientInfoException;
import java.sql.SQLException;
import java.sql.SQLWarning;
import java.sql.SQLXML;
import java.sql.Savepoint;
import java.sql.Statement;
import java.sql.Struct;
import java.util.Map;
import java.util.Properties;
import java.util.concurrent.Executor;

/**
 * Proxy to be returned when connection is requested from JADBPool.
 * 
 * This proxy is required to have the ability of check when the resource is
 * closed. Instance of behave like the default java.sql.Connection close, the
 * item will be returned to the pool and this proxy will be mark as invalid so
 * no following calls can be made after the item is returned to pool.
 */
public class JADBPoolProxyConnection implements Connection {

	private boolean valid = true;
	private JADBPoolConnection jadbConn;
	private Connection conn;

	/**
	 * Will construct a proxied JADBPoolConnection.
	 * 
	 * All calls (except close()) will be proxied to the underlying
	 * java.sql.Connection.
	 * 
	 * @param connection
	 *            the item to be proxied
	 */
	public JADBPoolProxyConnection(JADBPoolConnection connection) {
		this.jadbConn = connection;
		this.conn = connection.getConnection();
	}

	@Override
	public <T> T unwrap(Class<T> iface) throws SQLException {
		checkValid();
		return conn.unwrap(iface);
	}

	@Override
	public boolean isWrapperFor(Class<?> iface) throws SQLException {
		checkValid();
		return conn.isWrapperFor(iface);
	}

	@Override
	public Statement createStatement() throws SQLException {
		checkValid();
		return conn.createStatement();
	}

	@Override
	public PreparedStatement prepareStatement(String sql) throws SQLException {
		checkValid();
		return conn.prepareStatement(sql);
	}

	@Override
	public CallableStatement prepareCall(String sql) throws SQLException {
		checkValid();
		return conn.prepareCall(sql);
	}

	@Override
	public String nativeSQL(String sql) throws SQLException {
		checkValid();
		return conn.nativeSQL(sql);
	}

	@Override
	public void setAutoCommit(boolean autoCommit) throws SQLException {
		checkValid();
		conn.setAutoCommit(autoCommit);
	}

	@Override
	public boolean getAutoCommit() throws SQLException {
		checkValid();
		return conn.getAutoCommit();
	}

	@Override
	public void commit() throws SQLException {
		checkValid();
		conn.commit();
	}

	@Override
	public void rollback() throws SQLException {
		checkValid();
		conn.rollback();
	}

	@Override
	public void close() throws SQLException {
		checkValid();
		try {
			valid = false;
			jadbConn.close();
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

	@Override
	public boolean isClosed() throws SQLException {
		checkValid();
		return conn.isClosed();
	}

	@Override
	public DatabaseMetaData getMetaData() throws SQLException {
		checkValid();
		return conn.getMetaData();
	}

	@Override
	public void setReadOnly(boolean readOnly) throws SQLException {
		checkValid();
		conn.setReadOnly(readOnly);
	}

	@Override
	public boolean isReadOnly() throws SQLException {
		checkValid();
		return conn.isReadOnly();
	}

	@Override
	public void setCatalog(String catalog) throws SQLException {
		checkValid();
		conn.setCatalog(catalog);
	}

	@Override
	public String getCatalog() throws SQLException {
		checkValid();
		return conn.getCatalog();
	}

	@Override
	public void setTransactionIsolation(int level) throws SQLException {
		checkValid();
		conn.setTransactionIsolation(level);
	}

	@Override
	public int getTransactionIsolation() throws SQLException {
		checkValid();
		return conn.getTransactionIsolation();
	}

	@Override
	public SQLWarning getWarnings() throws SQLException {
		checkValid();
		return conn.getWarnings();
	}

	@Override
	public void clearWarnings() throws SQLException {
		checkValid();
		conn.clearWarnings();
	}

	@Override
	public Statement createStatement(int resultSetType, int resultSetConcurrency) throws SQLException {
		checkValid();
		return conn.createStatement(resultSetType, resultSetConcurrency);
	}

	@Override
	public PreparedStatement prepareStatement(String sql, int resultSetType, int resultSetConcurrency)
			throws SQLException {
		checkValid();
		return conn.prepareStatement(sql, resultSetType, resultSetConcurrency);
	}

	@Override
	public CallableStatement prepareCall(String sql, int resultSetType, int resultSetConcurrency) throws SQLException {
		checkValid();
		return conn.prepareCall(sql, resultSetType, resultSetConcurrency);
	}

	@Override
	public Map<String, Class<?>> getTypeMap() throws SQLException {
		checkValid();
		return conn.getTypeMap();
	}

	@Override
	public void setTypeMap(Map<String, Class<?>> map) throws SQLException {
		checkValid();
		conn.setTypeMap(map);
	}

	@Override
	public void setHoldability(int holdability) throws SQLException {
		checkValid();
		conn.setHoldability(holdability);
	}

	@Override
	public int getHoldability() throws SQLException {
		checkValid();
		return conn.getHoldability();
	}

	@Override
	public Savepoint setSavepoint() throws SQLException {
		checkValid();
		return conn.setSavepoint();
	}

	@Override
	public Savepoint setSavepoint(String name) throws SQLException {
		checkValid();
		return conn.setSavepoint(name);
	}

	@Override
	public void rollback(Savepoint savepoint) throws SQLException {
		checkValid();
		conn.rollback(savepoint);
	}

	@Override
	public void releaseSavepoint(Savepoint savepoint) throws SQLException {
		checkValid();
		conn.releaseSavepoint(savepoint);
	}

	@Override
	public Statement createStatement(int resultSetType, int resultSetConcurrency, int resultSetHoldability)
			throws SQLException {
		checkValid();
		return conn.createStatement(resultSetType, resultSetConcurrency, resultSetHoldability);
	}

	@Override
	public PreparedStatement prepareStatement(String sql, int resultSetType, int resultSetConcurrency,
			int resultSetHoldability) throws SQLException {
		checkValid();
		return conn.prepareStatement(sql, resultSetType, resultSetConcurrency, resultSetHoldability);
	}

	@Override
	public CallableStatement prepareCall(String sql, int resultSetType, int resultSetConcurrency,
			int resultSetHoldability) throws SQLException {
		checkValid();
		return conn.prepareCall(sql, resultSetType, resultSetConcurrency, resultSetHoldability);
	}

	@Override
	public PreparedStatement prepareStatement(String sql, int autoGeneratedKeys) throws SQLException {
		checkValid();
		return conn.prepareStatement(sql, autoGeneratedKeys);
	}

	@Override
	public PreparedStatement prepareStatement(String sql, int[] columnIndexes) throws SQLException {
		checkValid();
		return conn.prepareStatement(sql, columnIndexes);
	}

	@Override
	public PreparedStatement prepareStatement(String sql, String[] columnNames) throws SQLException {
		checkValid();
		return conn.prepareStatement(sql, columnNames);
	}

	@Override
	public Clob createClob() throws SQLException {
		checkValid();
		return conn.createClob();
	}

	@Override
	public Blob createBlob() throws SQLException {
		checkValid();
		return conn.createBlob();
	}

	@Override
	public NClob createNClob() throws SQLException {
		checkValid();
		return conn.createNClob();
	}

	@Override
	public SQLXML createSQLXML() throws SQLException {
		checkValid();
		return conn.createSQLXML();
	}

	@Override
	public boolean isValid(int timeout) throws SQLException {
		checkValid();
		return conn.isValid(timeout);
	}

	@Override
	public void setClientInfo(String name, String value) throws SQLClientInfoException {
		checkValid();
		conn.setClientInfo(name, value);
	}

	@Override
	public void setClientInfo(Properties properties) throws SQLClientInfoException {
		checkValid();
		conn.setClientInfo(properties);
	}

	@Override
	public String getClientInfo(String name) throws SQLException {
		checkValid();
		return conn.getClientInfo(name);
	}

	@Override
	public Properties getClientInfo() throws SQLException {
		checkValid();
		return conn.getClientInfo();
	}

	@Override
	public Array createArrayOf(String typeName, Object[] elements) throws SQLException {
		checkValid();
		return conn.createArrayOf(typeName, elements);
	}

	@Override
	public Struct createStruct(String typeName, Object[] attributes) throws SQLException {
		checkValid();
		return conn.createStruct(typeName, attributes);
	}

	@Override
	public void setSchema(String schema) throws SQLException {
		checkValid();
		conn.setSchema(schema);
	}

	@Override
	public String getSchema() throws SQLException {
		checkValid();
		return conn.getSchema();
	}

	@Override
	public void abort(Executor executor) throws SQLException {
		checkValid();
		conn.abort(executor);
	}

	@Override
	public void setNetworkTimeout(Executor executor, int milliseconds) throws SQLException {
		checkValid();
		conn.setNetworkTimeout(executor, milliseconds);
	}

	@Override
	public int getNetworkTimeout() throws SQLException {
		checkValid();
		return conn.getNetworkTimeout();
	}

	private void checkValid() {
		if (!valid)
			throw new RuntimeException("Holding to an object already returned to pool");
	}

}
