package com.alexgova.jadbpool;

/**
 * Custom exception to be used in Pool base classes.
 */
public class PoolException extends RuntimeException {

	private static final long serialVersionUID = 7482699362229830633L;

	public PoolException() {
		super();
	}

	public PoolException(String message) {
		super(message);
	}

	public PoolException(String message, Throwable throwable) {
		super(message, throwable);
	}
}
