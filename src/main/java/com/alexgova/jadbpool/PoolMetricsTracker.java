package com.alexgova.jadbpool;

import java.util.concurrent.atomic.AtomicInteger;
import java.util.concurrent.atomic.AtomicLong;

/**
 * To record the required pool stats.
 */
public class PoolMetricsTracker<T extends PoolItem> {
	private AtomicInteger itemsDisposed = new AtomicInteger(0);
	private AtomicInteger itemsLeased = new AtomicInteger(0);
	private AtomicInteger itemsLeasedReturned = new AtomicInteger(0);
	private AtomicLong waitForLeasingTime = new AtomicLong(0);
	private AtomicLong leasingTime = new AtomicLong(0);

	public void recordItemDisposed() {
		itemsDisposed.incrementAndGet();
	}

	public void recordItemLeased(long waitForLeasingTime) {
		itemsLeased.incrementAndGet();
		this.waitForLeasingTime.addAndGet(waitForLeasingTime);
	}

	public void recordLeasedItemReturn(long leasingTime) {
		itemsLeasedReturned.incrementAndGet();
		this.leasingTime.addAndGet(leasingTime);
	}

	public int getItemsDisposed() {
		return itemsDisposed.get();
	}

	public float getMeanLeaseTime() {
		if (itemsLeasedReturned.get() == 0) {
			return 0;
		}
		return (float) leasingTime.get() / (float) itemsLeasedReturned.get();
	}

	public float getMeanWaitTime() {
		if (itemsLeased.get() == 0) {
			return 0;
		}
		return (float) waitForLeasingTime.get() / (float) itemsLeased.get();
	}

	public void printStats(Pool<T> pool) {
		System.out.println("/************** Pool Stats ********************/");
		System.out.println("Total items in pool: " + pool.size());
		System.out.println("Available items: " + pool.getCountByState(PoolItemState.AVAILABLE));
		System.out.println("Unavailable items: " + pool.getCountByState(PoolItemState.UNAVAILABLE));
		System.out.println("Items/Connections killed: " + getItemsDisposed());
		System.out.println("Mean lease time (ms): " + getMeanLeaseTime());
		System.out.println("Mean wait time (ms): " + getMeanWaitTime());
		System.out.println("/************** Pool Stats ********************/");
	}

}
