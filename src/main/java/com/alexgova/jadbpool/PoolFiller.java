package com.alexgova.jadbpool;

import java.util.concurrent.ExecutorService;

/**
 * Maintenance task used to keep the cache of available items in the pool.
 */
final class PoolFiller<T extends PoolItem> extends PoolMaintenanceTask<T> {

	public PoolFiller(Pool<T> pool, PoolConfiguration config, ExecutorService executor) {
		super(pool, config, executor);
	}

	@Override
	public void run() {
		int itemsToCreate = Math.min(config.getMaxCapacity() - pool.size(),
				config.getMinAvailableItems() - pool.getCountByState(PoolItemState.AVAILABLE));
		for (int i = 0; i < itemsToCreate && pool.size() < config.getMaxCapacity(); i++) {
			executor.submit(() -> pool.addItemToPool(pool.createItem()));
		}

		awaitTermination();
	}

}
