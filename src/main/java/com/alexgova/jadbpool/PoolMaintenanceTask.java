package com.alexgova.jadbpool;

import java.util.concurrent.ExecutorService;
import java.util.concurrent.TimeUnit;

public abstract class PoolMaintenanceTask<T extends PoolItem> implements Runnable {

	protected final Pool<T> pool;
	protected final PoolConfiguration config;
	protected final ExecutorService executor;

	public PoolMaintenanceTask(Pool<T> pool, PoolConfiguration config, ExecutorService executor) {
		this.pool = pool;
		this.config = config;
		this.executor = executor;
	}

	protected void awaitTermination() {
		// wait for child tasks to finish, otherwise will mess up the maintainer
		try {
			executor.awaitTermination(100, TimeUnit.MILLISECONDS);
		} catch (InterruptedException e) {
			Thread.currentThread().interrupt();
		}
	}

}
