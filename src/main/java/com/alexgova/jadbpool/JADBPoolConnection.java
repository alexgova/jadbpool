package com.alexgova.jadbpool;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;

/**
 * This class extends PoolItem to get the ability to be handled from the Pool
 * and adds functionality to know how to create connections to the database.
 */
public class JADBPoolConnection extends PoolItem {

	private JADBPoolConfiguration config;
	private Connection connection;

	/**
	 * Constructs a JADBPoolConnection with an underlying java.sql.Connection based
	 * on the configuration set by a JADBPoolConfiguration instance.
	 * 
	 * @param config
	 *            the configuration to be used when creating connections.
	 * @param pool
	 *            the Pool this item belongs to.
	 */
	public JADBPoolConnection(JADBPoolConfiguration config, Pool pool) {
		super(pool);
		this.config = config;
		try {
			connection = getNewConnection();
		} catch (SQLException e) {
			throw new JADBPoolConnectionException("can't get new connection", e);
		}
	}

	/**
	 * Gets a new connection based on the configuration stored in config.
	 * 
	 * @return java.sql.Connection
	 * @throws SQLException
	 *             if no connection could be created.
	 */
	private Connection getNewConnection() throws SQLException {

		return config.getUser() == null ? DriverManager.getConnection(config.getDatabaseUrl())
				: DriverManager.getConnection(config.getDatabaseUrl(), config.getUser(), config.getPassword());
	}

	/**
	 * Gets the underlying java.sql.Connection of this item.
	 * 
	 * @return java.sql.Connection the underlying connection of this item.
	 */
	public Connection getConnection() {
		return connection;
	}

	/**
	 * Validates if the connection is still usable for future usage.
	 */
	@Override
	public boolean isValid() {
		try {
			if (connection.isValid(100))
				return true;
			else {
				connection.close();
				return false;
			}
		} catch (SQLException e) {
			e.printStackTrace();
		}
		return false;
	}

	/**
	 * Will get rid of connection resources used by the underlying connection.
	 */
	@Override
	public void dispose() {
		try {
			if (connection.isValid(100))
				connection.close();
		} catch (SQLException e) {
			e.printStackTrace();
		}
	}
}
