package com.alexgova.jadbpool;

import java.util.List;
import java.util.concurrent.ExecutorService;

/**
 * Maintenance task used to clean all resources used by the pool.
 */
final class PoolAllCleaner<T extends PoolItem> extends PoolMaintenanceTask<T> implements Runnable {

	public PoolAllCleaner(Pool<T> pool, PoolConfiguration config, ExecutorService executor) {
		super(pool, config, executor);
	}

	@Override
	public void run() {
		List<T> items = pool.getAllItems();
		for (int i = 0; i < items.size(); i++) {
			final T item = items.get(i);
			executor.submit(() -> pool.removeItem(item));
		}

		awaitTermination();
	}

}
