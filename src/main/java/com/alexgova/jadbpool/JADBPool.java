package com.alexgova.jadbpool;

import java.sql.Connection;

/**
 * Just Another DB Pool, extends the functionality from Pool to handle
 * connection pooling.
 */
public class JADBPool extends Pool<JADBPoolConnection> {

	private final JADBPoolConfiguration config;

	/**
	 * To create a new JADBPool, an instance of JADBPoolConfiguration needs to be
	 * passed to know which configuration to use in the pool creation.
	 * 
	 * @param config
	 *            configuration to be used in the JADBPool creation.
	 */
	public JADBPool(JADBPoolConfiguration config) {
		super(config);
		// TODO validate configuration
		this.config = config;
		// TODO there's a bug when fillPool is called from the parent due to config not
		// available at creation time of items, need to find a workaround on it
		fillPool();
	}

	/**
	 * Will retrieve an item available from the Pool and will wrap it in a
	 * JADBPoolProxyConnection to be able to validate if the client attempts
	 * operations on the connection after it has been returned to the pool
	 * 
	 * @return Connection
	 */
	public Connection getConnection() {

		JADBPoolConnection connection = getItem();
		return new JADBPoolProxyConnection(connection);

	}

	/**
	 * The method the pool will use to create the necessary resources for the
	 * connection
	 * 
	 * @return JADBPoolConnection
	 */
	@Override
	protected JADBPoolConnection createItem() {
		return new JADBPoolConnection(config, this);
	}

}
