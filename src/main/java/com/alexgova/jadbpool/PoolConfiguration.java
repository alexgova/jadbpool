package com.alexgova.jadbpool;

/**
 * Basic pool configuration.
 */
public class PoolConfiguration {

	private int maxCapacity;
	private int minAvailableItems;

	/**
	 * Gets the max capacity allowed for the pool.
	 * 
	 * @return int the max capacity allowed for the pool.
	 */
	public int getMaxCapacity() {
		return maxCapacity;
	};

	/**
	 * Sets the max capacity allowed for the pool.
	 * 
	 * @param capacity
	 *            the max capacity allowed for the pool.
	 */
	public void setMaxCapacity(int capacity) {
		maxCapacity = capacity;
	};

	/**
	 * Gets the minimum amount of items to be available in the pool.
	 * 
	 * @return int the minimum amount of items to be available in the pool.
	 */
	public int getMinAvailableItems() {
		return minAvailableItems;
	}

	/**
	 * Sets the minimum amount of items to be available in the pool.
	 * 
	 * @param amount
	 *            the minimum amount of items to be available in the pool.
	 */
	public void setMinAvailableItems(int amount) {
		minAvailableItems = amount;
	};

}
