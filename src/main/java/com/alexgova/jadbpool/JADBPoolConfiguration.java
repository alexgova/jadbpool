package com.alexgova.jadbpool;

/**
 * Configuration specific to database pooling. It will be used in JADBPool to
 * know how to create connections to database.
 */
public class JADBPoolConfiguration extends PoolConfiguration {

	private String databaseUrl;
	private String user;
	private String password;

	/**
	 * Gets the database url.
	 * 
	 * @return String the database url.
	 */
	public String getDatabaseUrl() {
		return databaseUrl;
	}

	/**
	 * Sets the database url
	 * 
	 * @param url
	 *            the database url that will be used to create connections.
	 */
	public void setDatabaseUrl(String url) {
		this.databaseUrl = url;
	}

	/**
	 * Gets the user used to connect to the database.
	 * 
	 * @return String the user used to connect to the database.
	 */
	public String getUser() {
		return user;
	}

	/**
	 * Sets the user to be used to connect to the database.
	 * 
	 * @param user
	 *            the user to be used to connect to the database.
	 */
	public void setUser(String user) {
		this.user = user;
	}

	/**
	 * Gets the password used to connect to the database.
	 * 
	 * @return String the password used to connect to the database.
	 */
	public String getPassword() {
		return password;
	}

	/**
	 * Sets the password to be used to connect to the database.
	 * 
	 * @param password
	 *            the password to be used to connect to the database.
	 */
	public void setPassword(String password) {
		this.password = password;
	}

}
