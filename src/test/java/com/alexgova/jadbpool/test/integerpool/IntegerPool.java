package com.alexgova.jadbpool.test.integerpool;

import java.io.IOException;
import java.util.concurrent.TimeUnit;

import com.alexgova.jadbpool.Pool;
import com.alexgova.jadbpool.PoolConfiguration;

public class IntegerPool extends Pool<IntegerPoolItem> {

	public IntegerPool(PoolConfiguration config) {
		super(config);
		fillPool();
	}

	public IntegerPoolItem getInteger() {
		return getItem();
	}

	@Override
	public void close() throws IOException {
	}

	@Override
	protected IntegerPoolItem createItem() {
		int randomValue = (int) Math.round(Math.random() * 5);
		try {
			TimeUnit.MILLISECONDS.sleep(randomValue);
		} catch (InterruptedException e) {
			e.printStackTrace();
		}
		return new IntegerPoolItem(randomValue, this);
	}

}
