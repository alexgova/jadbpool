package com.alexgova.jadbpool.test.integerpool;

import com.alexgova.jadbpool.Pool;
import com.alexgova.jadbpool.PoolItem;

public class IntegerPoolItem extends PoolItem {

	private Integer value;

	public IntegerPoolItem(int value, Pool pool) {
		super(pool);
		this.value = new Integer(value);
	}

	public int getValue() {
		return value.intValue();
	}

	@Override
	public boolean isValid() {
		return true;
	}

	@Override
	public void dispose() {
		// No resources to dispose
	}

}
