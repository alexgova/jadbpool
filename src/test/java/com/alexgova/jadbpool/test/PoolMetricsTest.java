package com.alexgova.jadbpool.test;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

import java.io.IOException;
import java.util.concurrent.TimeUnit;

import org.junit.Test;

import com.alexgova.jadbpool.PoolConfiguration;
import com.alexgova.jadbpool.test.integerpool.IntegerPool;
import com.alexgova.jadbpool.test.integerpool.IntegerPoolItem;

public class PoolMetricsTest {

	@Test
	public void metricsTest() throws IOException, InterruptedException {
		PoolConfiguration config = new PoolConfiguration();
		config.setMinAvailableItems(5);
		config.setMaxCapacity(10);

		IntegerPool pool = new IntegerPool(config);

		assertEquals(5, pool.size());

		IntegerPoolItem items[] = new IntegerPoolItem[10];

		for (int i = 0; i < 10; i++) {
			items[i] = pool.getInteger();
		}

		TimeUnit.MILLISECONDS.sleep(500);

		for (int i = 0; i < 10; i++) {
			items[i].close();
		}

		TimeUnit.MILLISECONDS.sleep(500);

		assertEquals(5, pool.getItemsDisposed());
		assertTrue(pool.getMeanLeaseTime() > 0);
		assertTrue(pool.getMeanWaitTime() > 0);

		pool.printStats();

		pool.close();

	}

}
