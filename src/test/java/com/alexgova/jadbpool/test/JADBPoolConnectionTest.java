package com.alexgova.jadbpool.test;

import static org.junit.Assert.assertNotNull;

import java.io.IOException;

import org.junit.Test;

import com.alexgova.jadbpool.JADBPoolConfiguration;
import com.alexgova.jadbpool.JADBPoolConnection;
import com.alexgova.jadbpool.JADBPoolConnectionException;

public class JADBPoolConnectionTest {
	private static final String H2_DATABASE_URL = "jdbc:h2:mem:test";
	private static final String INVALID_DATABASE_URL = "jdbc:foo:foo";

	@SuppressWarnings("resource")
	@Test
	public void canCreateConnection() throws IOException {
		JADBPoolConfiguration config = new JADBPoolConfiguration();
		config.setDatabaseUrl(H2_DATABASE_URL);

		JADBPoolConnection poolConnection = new JADBPoolConnection(config, null);
		assertNotNull(poolConnection.getConnection());
		poolConnection.dispose();

	}

	@Test(expected = JADBPoolConnectionException.class)
	public void cantCreateConnectionWithInvalidUrl() throws IOException {
		JADBPoolConfiguration config = new JADBPoolConfiguration();
		config.setDatabaseUrl(INVALID_DATABASE_URL);

		try (JADBPoolConnection conn = new JADBPoolConnection(config, null)) {

		}
	}

}
