package com.alexgova.jadbpool.test;

import com.alexgova.jadbpool.Pool;
import com.alexgova.jadbpool.PoolItem;

class MockPoolItem extends PoolItem {

	public MockPoolItem(Pool pool) {
		super(pool);
	}

	@Override
	public boolean isValid() {
		return true;
	}

	@Override
	public void dispose() {
		// no resources to dispose
	}

}
