package com.alexgova.jadbpool.test;

import java.io.IOException;
import java.sql.Connection;
import java.sql.SQLException;

import org.junit.Test;

import com.alexgova.jadbpool.JADBPool;
import com.alexgova.jadbpool.JADBPoolConfiguration;

public class JADBPoolTest {

	private static final String H2_DATABASE_URL = "jdbc:h2:mem:test";

	@Test(expected = RuntimeException.class)
	public void shouldThrowExceptionIfConnectionReturned() throws IOException, SQLException {
		JADBPoolConfiguration config = new JADBPoolConfiguration();
		config.setMinAvailableItems(2);
		config.setMaxCapacity(2);
		config.setDatabaseUrl(H2_DATABASE_URL);

		JADBPool pool = new JADBPool(config);

		Connection connection = pool.getConnection();

		connection.close();
		connection.close(); // Trying to close connection or calling any other method after close will throw
							// exception

		pool.close();

	}

}
