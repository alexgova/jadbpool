package com.alexgova.jadbpool.test;

import static org.junit.Assert.assertEquals;

import java.io.IOException;

import org.junit.Test;

import com.alexgova.jadbpool.PoolException;
import com.alexgova.jadbpool.PoolItemState;

public class PoolTest {

	@Test
	public void canFillPoolToMinimum() throws IOException {
		int minItemsInPool = 5;
		MockPoolConfiguration config = new MockPoolConfiguration();
		config.setMinAvailableItems(minItemsInPool);

		try (MockPool pool = new MockPool(config)) {
			assertEquals(minItemsInPool, pool.getCountByState(PoolItemState.AVAILABLE));
		}
	}

	@Test(expected = PoolException.class)
	public void exceptionIfTryingToGetAfterMaxCapacity() throws IOException {
		int minItemsInPool = 2;
		int maxCapacity = 3;
		MockPoolConfiguration config = new MockPoolConfiguration();
		config.setMinAvailableItems(minItemsInPool);
		config.setMaxCapacity(maxCapacity);

		try (MockPool pool = new MockPool(config)) {
			for (int i = 0; i < maxCapacity + 1; i++) {
				@SuppressWarnings("unused")
				MockPoolItem item = pool.getMockPoolItem();
			}
		}
	}
}
