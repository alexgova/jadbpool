package com.alexgova.jadbpool.test;

import java.io.IOException;

import com.alexgova.jadbpool.Pool;

public class MockPool extends Pool<MockPoolItem> {

	protected MockPool(MockPoolConfiguration config) {
		super(config);
		fillPool();
	}

	public MockPoolItem getMockPoolItem() {
		return getItem();
	}

	@Override
	protected MockPoolItem createItem() {
		return new MockPoolItem(this);
	}

	@Override
	public void close() throws IOException {

	}

}
