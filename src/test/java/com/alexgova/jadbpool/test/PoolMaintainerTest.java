package com.alexgova.jadbpool.test;

import static com.alexgova.jadbpool.PoolItemState.AVAILABLE;
import static com.alexgova.jadbpool.PoolItemState.UNAVAILABLE;
import static org.junit.Assert.assertEquals;

import java.io.IOException;
import java.util.concurrent.TimeUnit;

import org.junit.Test;

import com.alexgova.jadbpool.PoolConfiguration;
import com.alexgova.jadbpool.test.integerpool.IntegerPool;
import com.alexgova.jadbpool.test.integerpool.IntegerPoolItem;

public class PoolMaintainerTest {

	@Test
	public void canMaintainMinimumAvailable() throws IOException, InterruptedException {
		PoolConfiguration config = new PoolConfiguration();
		config.setMinAvailableItems(5);
		config.setMaxCapacity(10);

		IntegerPool pool = new IntegerPool(config);

		assertEquals(5, pool.size());

		try (IntegerPoolItem item = pool.getInteger()) {

			assertEquals(4, pool.getCountByState(AVAILABLE));

			TimeUnit.MILLISECONDS.sleep(500);

			assertEquals(6, pool.size());
			assertEquals(5, pool.getCountByState(AVAILABLE));
			assertEquals(1, pool.getCountByState(UNAVAILABLE));
		}

		TimeUnit.MILLISECONDS.sleep(500);

		assertEquals(5, pool.size());
		assertEquals(5, pool.getCountByState(AVAILABLE));

		pool.close();

	}

	@Test
	public void canMaintainMinimumAvailableWithMax() throws IOException, InterruptedException {
		PoolConfiguration config = new PoolConfiguration();
		config.setMinAvailableItems(5);
		config.setMaxCapacity(10);

		IntegerPool pool = new IntegerPool(config);

		assertEquals(5, pool.size());

		IntegerPoolItem items[] = new IntegerPoolItem[8];

		for (int i = 0; i < 8; i++) {
			items[i] = pool.getInteger();
		}

		TimeUnit.MILLISECONDS.sleep(500);
		assertEquals(8, pool.getCountByState(UNAVAILABLE));
		assertEquals(2, pool.getCountByState(AVAILABLE));

		for (int i = 0; i < 8; i++) {
			items[i].close();
		}

		pool.close();
	}

}
